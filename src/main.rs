extern crate serde_json;
extern crate rmp_serde;

use std::env::args;
use std::path::PathBuf;
use std::io;
use std::io::prelude::*;
use std::fs::File;
use serde_json::Value;

fn read_msg_into_stdout<T:Read>(input: T) {
    let stdout = io::stdout();
    let input_msgpack : Value = rmp_serde::from_read(input).unwrap();
    serde_json::to_writer_pretty(stdout, &input_msgpack).unwrap();
}

const HELP_STR : &'static str = r#"msgpack-cli-viewer 1.0.0
View the content of messagePack formatted files.

Usage:
 view-msgpk --help | -h : display this help
 view-msgpk --version | -v : display version
 view-msgpk [file] : show in stdout the content of
                     messagePack formatted `file`
 view-msgpk : take messagePack formatted stream as
              input, outputs the pretty-formatted
              JSON value.

License:
 ??? Whatever you want, it's about 20 lines of code
 anyone can write that. It's like a recipe for cake, do
 you think cake recipes should be copyrighted??????
 (officially this is MIT | APL2 because I want to follow
 the same scheme as the libraries I use)
"#;

fn main() {
    let file_to_read = args().nth(1);
    match file_to_read {
        Some(x) => match x.as_ref() {
            "--help" | "-h" => println!("{}", HELP_STR),
            "--version" | "-v" => println!("msgpack-cli-viewer 1.0.0"),
            anyelse => {
                let path = PathBuf::from(anyelse);
                read_msg_into_stdout(File::open(path).unwrap())
            },
        },
        None => read_msg_into_stdout(io::stdin()),
    };
}
